package handlers

import (
	"gitlab.com/eng-bot/bot/common"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
)

var NewUserHandler = models.NewDefaultMessageHandler(
	"Вас приветствует бот для итеративного запоминания иностранных слов. \nВыберите желаемое действие",
	common.BeginState,
)

var AddWordHandler = models.NewDefaultMessageHandler(
	"Введите перевод для слова",
	common.AddTranslationState,
)

var AddTranslationUserHandler = models.NewDefaultMessageHandler(
	"Выберите желаемое действие",
	common.BeginState,
)
