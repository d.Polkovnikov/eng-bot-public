package models

import (
	"gitlab.com/eng-bot/bot/internal/telegramclient"
	"reflect"
	"testing"
)

const (
	okState    = ApplicationState("OK")
	nextState  = ApplicationState("NEXT_STATE")
	errState   = ApplicationState("ERR_STATE")
	defaultMsg = "MSG"
)

func TestDefaultHandlerConfig_GetHandler(t *testing.T) {
	handlerConfig := NewDefaultHandlerConfig(map[ApplicationState]MessageHandler{
		okState: NewDefaultMessageHandler(defaultMsg, nextState),
	})
	type result struct {
		msg   string
		state ApplicationState
	}
	tests := []struct {
		name       string
		inputState ApplicationState
		want       result
		wantErr    bool
	}{
		{
			name:       "GetHandler should return default handler",
			inputState: okState,
			want:       result{msg: defaultMsg, state: nextState},
			wantErr:    false,
		},
		{
			name:       "GetHandler should return error",
			inputState: errState,
			want:       result{msg: defaultMsg, state: nextState},
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handler, err := handlerConfig.GetHandler(tt.inputState)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetHandler() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if handler == nil {
				if err == nil {
					t.Errorf("Both error and handler are nill")
				}
				return
			}

			msg, state, _ := handler.Handle(telegramclient.Update{})
			if msg != tt.want.msg {
				t.Errorf("handler.Handle() message =  %s, wantMsg %s", msg, tt.want.msg)
			}
			if state != tt.want.state {
				t.Errorf("handler.Handle() state =  %s, wantState %s", state, tt.want.state)
			}
		})
	}
}

func TestDefaultKeyboardConfig_GetKeyboard(t *testing.T) {
	type fields struct {
		defaultKeyboard interface{}
		config          map[ApplicationState]interface{}
	}
	const okState = ApplicationState("STATE")
	const errState = ApplicationState("ERR_STATE")
	myFields := fields{
		defaultKeyboard: telegramclient.ReplyKeyboardHide{},
		config: map[ApplicationState]interface{}{
			okState: telegramclient.ReplyKeyboardMarkup{},
		},
	}
	tests := []struct {
		name   string
		fields fields
		state  ApplicationState
		want   interface{}
	}{
		{
			"GetKeyboard() should return ReplyKeyboardMarkup",
			myFields,
			okState,
			telegramclient.ReplyKeyboardMarkup{},
		}, {
			"GetKeyboard() should return ReplyKeyboardHide",
			myFields,
			errState,
			telegramclient.ReplyKeyboardHide{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			kc := &DefaultKeyboardConfig{
				defaultKeyboard: tt.fields.defaultKeyboard,
				config:          tt.fields.config,
			}
			if got := kc.GetKeyboard(tt.state); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetKeyboard() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultMessageConfig_Handle(t *testing.T) {
	handler := NewDefaultMessageHandler(defaultMsg, okState)
	message, state, err := handler.Handle(telegramclient.Update{})

	if err != nil {
		t.Errorf("DefaultMessageHandler.Handle() return error, want not error")
	} else {
		if defaultMsg != message {
			t.Errorf("DefaultMessageHandler.Handle().message = %s, want %s", message, defaultMsg)
		}
		if okState != state {
			t.Errorf("DefaultMessageHandler.Handle().state = %v, want %v", state, okState)
		}
	}
}
