package core

import (
	"context"
	"fmt"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
)

type Server struct {
	requestHandlers *mainMsgHandler
	tgClient        telegramclient.TGClient
	errors          chan error
	updates         chan telegramclient.Update
	messages        chan telegramclient.SendMessageRequest
}

func CreateServer(config models.MainConfig) (*Server, error) {
	mainHandler, err := NewMainHandler(
		config.KeyboardConfig,
		config.HandlerConfig,
	)
	if err != nil {
		return nil, err
	}

	tgClient, err := telegramclient.NewTGClient(config.TelegramConfig)
	if err != nil {
		return nil, err
	}
	return &Server{
		requestHandlers: mainHandler,
		tgClient:        tgClient,
		updates:         make(chan telegramclient.Update),
		errors:          make(chan error),
		messages:        make(chan telegramclient.SendMessageRequest),
	}, nil
}

func (s *Server) Start(ctx context.Context) {
	go s.startHandlingErrors(ctx)
	go s.startHandlingMessages(ctx)
	go s.startHandlingUpdates(ctx)
	go s.startReadUpdates(ctx)
}

func (s *Server) startReadUpdates(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			updates, err := s.tgClient.GetNextMessages()
			if err != nil {
				s.errors <- err
				continue
			}
			for _, update := range updates {
				s.updates <- update
			}
		}
	}
}

func (s *Server) startHandlingUpdates(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case update := <-s.updates:
			go func() {
				handle, err := s.requestHandlers.Handle(update)
				if err != nil {
					s.errors <- err
				} else {
					s.messages <- *handle
				}
			}()
		}
	}
}

func (s *Server) startHandlingMessages(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-s.messages:
			err := s.tgClient.SendMessage(msg)
			if err != nil {
				s.errors <- err
			}
		}
	}
}

func (s *Server) startHandlingErrors(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case err := <-s.errors:
			fmt.Printf("ERROR:%s\n", err.Error())
		}
	}
}
