package models

type ApplicationState string

const NewState ApplicationState = "NEW"
const EmptyState ApplicationState = "EMPTY"
