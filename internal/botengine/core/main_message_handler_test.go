package core

import (
	"errors"
	"github.com/golang/mock/gomock"
	botengine_mocks "gitlab.com/eng-bot/bot/internal/botengine/mocks"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
	"reflect"
	"testing"
)

func Test_mainMsgHandler_Handle(t *testing.T) {
	update := telegramclient.Update{
		UpdateId: 0,
		Message: telegramclient.Message{
			Id: 0,
			From: telegramclient.User{
				Id: 10,
			},
			Text: "",
		},
	}

	type handlerConfigAnswer struct {
		response models.MessageHandler
		err      error
	}

	type userStatesServiceAnswer struct {
		getUserStateResponse models.ApplicationState
		getUserStateErr      error
		updateStateErr       error
	}

	tests := []struct {
		name                    string
		userStatesServiceAnswer userStatesServiceAnswer
		handlerConfigAnswer     handlerConfigAnswer
		want                    *telegramclient.SendMessageRequest
		wantErr                 bool
		wantErrMsg              string
	}{
		{
			"Hanlde should return err when GetUserState return error",
			userStatesServiceAnswer{
				getUserStateResponse: "",
				getUserStateErr:      errors.New("error1"),
			},
			handlerConfigAnswer{},
			nil,
			true,
			"error1",
		},
		{
			"Hanlde should return err when GetHandler return error",
			userStatesServiceAnswer{
				getUserStateResponse: models.NewState,
			},
			handlerConfigAnswer{
				response: nil,
				err:      errors.New("error2"),
			},
			nil,
			true,
			"error2",
		},
		{
			"Handle should return err when MessageHandler return error",
			userStatesServiceAnswer{
				getUserStateResponse: models.NewState,
			},
			handlerConfigAnswer{
				response: &ErrMessageHandler{},
			},
			nil,
			true,
			"msgError",
		},
		{
			"Handle should return err when UpdateState return error",
			userStatesServiceAnswer{
				getUserStateResponse: models.NewState,
				updateStateErr:       errors.New("error3"),
			},
			handlerConfigAnswer{
				response: models.NewDefaultMessageHandler("message", models.NewState),
			},
			nil,
			true,
			"error3",
		},
		{
			"Handler should return answer",
			userStatesServiceAnswer{
				getUserStateResponse: models.NewState,
			},
			handlerConfigAnswer{
				response: models.NewDefaultMessageHandler("message", models.NewState),
			},
			&telegramclient.SendMessageRequest{
				ChatId:         "10",
				Text:           "message",
				MessageToReply: 0,
				ReplyMarkup:    telegramclient.ReplyKeyboardHide{HideKeyboard: true},
			},
			false,
			"",
		},
	}
	for _, tt := range tests {
		ctrl := gomock.NewController(t)
		handlerConfig := botengine_mocks.NewMockHandlerConfig(ctrl)
		userStatesService := botengine_mocks.NewMockUserStatesService(ctrl)
		mh := &mainMsgHandler{
			handlerConfig:  handlerConfig,
			keyboardConfig: models.NewDefaultKeyboardConfig(map[models.ApplicationState]interface{}{}),
			userStates:     userStatesService,
		}
		t.Run(tt.name, func(t *testing.T) {
			userStatesService.EXPECT().
				GetUserState(gomock.Any()).
				Return(tt.userStatesServiceAnswer.getUserStateResponse, tt.userStatesServiceAnswer.getUserStateErr).
				MaxTimes(1)

			handlerConfig.EXPECT().
				GetHandler(gomock.Any()).
				Return(tt.handlerConfigAnswer.response, tt.handlerConfigAnswer.err).
				MaxTimes(1)

			userStatesService.EXPECT().
				UpdateState(gomock.Any(), gomock.Any()).
				Return(tt.userStatesServiceAnswer.updateStateErr).
				MaxTimes(1)

			got, err := mh.Handle(update)
			if (err != nil) != tt.wantErr {
				t.Errorf("Handle() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (err != nil) && tt.wantErr && err.Error() != tt.wantErrMsg {
				t.Errorf("Handle() errorMsg = %s, wantErrMsg %s", err.Error(), tt.wantErrMsg)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Handle() got = %v, want %v", got, tt.want)
			}
		})
		ctrl.Finish()
	}
}

type ErrMessageHandler struct{}

func (dh *ErrMessageHandler) Handle(update telegramclient.Update) (string, models.ApplicationState, error) {
	return "", models.EmptyState, errors.New("msgError")
}
