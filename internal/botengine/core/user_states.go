package core

import (
	"errors"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"strconv"
)

type UserStatesService interface {
	GetUserState(int64) (models.ApplicationState, error)
	UpdateState(int64, models.ApplicationState) error
}

type LocalUserStatesService struct {
	states map[int64]models.ApplicationState
}

func NewLocalUserStatesService() UserStatesService {
	return &LocalUserStatesService{
		states: make(map[int64]models.ApplicationState),
	}
}

func (us *LocalUserStatesService) GetUserState(userId int64) (models.ApplicationState, error) {
	state, ok := us.states[userId]
	if ok {
		return state, nil
	}
	us.states[userId] = models.NewState
	return models.NewState, nil
}

func (us *LocalUserStatesService) UpdateState(userId int64, newState models.ApplicationState) error {
	_, ok := us.states[userId]
	if !ok {
		return errors.New("Unknown user ID: " + strconv.FormatInt(userId, 10))
	}
	us.states[userId] = newState
	return nil
}
