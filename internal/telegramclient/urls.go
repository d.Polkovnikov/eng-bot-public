package telegramclient

import (
	"fmt"
	url2 "net/url"
	"strconv"
)

type ClientUrls struct {
	ClientConfig
	getUpdatesUrl  string
	sendMessageUrl string
}

func initClientUrls(config ClientConfig) *ClientUrls {
	baseUrl := fmt.Sprintf("%s/bot%s", config.BaseUrl, config.Token)
	return &ClientUrls{
		getUpdatesUrl:  fmt.Sprintf("%s/%s", baseUrl, "getUpdates"),
		sendMessageUrl: fmt.Sprintf("%s/%s", baseUrl, "sendMessage"),
	}
}

func (cu *ClientUrls) formatGetUpdatesUrl() string {
	v := url2.Values{}
	v.Add("timeout", strconv.Itoa(cu.Timeout))
	v.Add("offset", strconv.FormatInt(cu.Offset, 10))
	v.Add("limit", strconv.Itoa(cu.Limit))
	return fmt.Sprintf("%s?%s", cu.getUpdatesUrl, v.Encode())
}
