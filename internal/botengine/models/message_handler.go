package models

import (
	"fmt"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
)

type MessageHandler interface {
	Handle(update telegramclient.Update) (string, ApplicationState, error)
}

type DefaultMessageHandler struct {
	defaultMessage   string
	defaultNextState ApplicationState
}

func (dh *DefaultMessageHandler) Handle(update telegramclient.Update) (string, ApplicationState, error) {
	fmt.Printf("%v\n", update)
	return dh.defaultMessage, dh.defaultNextState, nil
}

func NewDefaultMessageHandler(defaultMessage string, defaultNextState ApplicationState) MessageHandler {
	return &DefaultMessageHandler{
		defaultMessage:   defaultMessage,
		defaultNextState: defaultNextState,
	}
}
