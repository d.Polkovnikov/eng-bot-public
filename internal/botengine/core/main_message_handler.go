package core

import (
	"errors"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
	"strconv"
)

type mainMsgHandler struct {
	handlerConfig  models.HandlerConfig
	keyboardConfig models.KeyboardConfig
	userStates     UserStatesService
}

func NewMainHandler(
	keyboardConfig models.KeyboardConfig,
	handlerConfig models.HandlerConfig,
) (*mainMsgHandler, error) {
	handler, _ := handlerConfig.GetHandler(models.NewState)
	if handler == nil {
		return nil, errors.New("No handler for NewUser state")
	}
	return &mainMsgHandler{
		userStates:     NewLocalUserStatesService(),
		keyboardConfig: keyboardConfig,
		handlerConfig:  handlerConfig,
	}, nil
}

func (mh *mainMsgHandler) Handle(update telegramclient.Update) (*telegramclient.SendMessageRequest, error) {
	userId := update.Message.From.Id
	state, err := mh.userStates.GetUserState(userId)
	if err != nil {
		return nil, err
	}

	messageHandler, err := mh.handlerConfig.GetHandler(state)
	if err != nil {
		return nil, err
	}

	replyMessage, nextState, err := messageHandler.Handle(update)
	if err != nil {
		return nil, err
	}
	replyMarkup := mh.keyboardConfig.GetKeyboard(nextState)

	response := telegramclient.SendMessageRequest{
		ChatId:      strconv.FormatInt(update.Message.From.Id, 10),
		Text:        replyMessage,
		ReplyMarkup: replyMarkup,
	}
	if err = mh.userStates.UpdateState(userId, nextState); err != nil {
		// Todo: need rollback all transaction
		return nil, err
	}
	return &response, nil
}
