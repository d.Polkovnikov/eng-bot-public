package core

import (
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"testing"
)

const customState = models.ApplicationState("CUSTOM")

func TestNewLocalUserStatesService(t *testing.T) {
	userStates := NewLocalUserStatesService()
	if len(userStates.(*LocalUserStatesService).states) != 0 {
		t.Errorf("init user states length != 0")
	}
}

func TestLocalUserStatesService_GetUserState(t *testing.T) {
	type fields struct {
		states map[int64]models.ApplicationState
	}
	type args struct {
		userId int64
	}
	tests := []struct {
		name   string
		fields fields
		userId int64
		want   models.ApplicationState
	}{
		{
			"should return new state for unknown user",
			fields{states: map[int64]models.ApplicationState{}},
			10,
			models.NewState,
		},
		{
			"should return custom state for known user",
			fields{states: map[int64]models.ApplicationState{10: customState}},
			10,
			customState,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			us := &LocalUserStatesService{
				states: tt.fields.states,
			}
			if got, _ := us.GetUserState(tt.userId); got != tt.want {
				t.Errorf("getUserState() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLocalUserStatesService_UpdateState(t *testing.T) {
	type fields struct {
		states map[int64]models.ApplicationState
	}
	type args struct {
		userId   int64
		newState models.ApplicationState
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"should update state for known user",
			fields{states: map[int64]models.ApplicationState{10: models.NewState}},
			args{
				userId:   10,
				newState: customState,
			},
			false,
		},
		{
			"should return err for unknown user",
			fields{states: map[int64]models.ApplicationState{}},
			args{1, customState},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			us := &LocalUserStatesService{
				states: tt.fields.states,
			}
			err := us.UpdateState(tt.args.userId, tt.args.newState)
			if (err != nil) != tt.wantErr {
				t.Errorf("updateState() error = %v, wantErr %v", err, tt.wantErr)
			}
			got, _ := us.GetUserState(tt.args.userId)
			if err == nil && got != tt.args.newState {
				t.Errorf("getUserState() = %v, want %v", got, tt.args.newState)
			}
		})
	}
}
