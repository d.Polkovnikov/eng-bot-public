package telegramclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"sync"
)

const (
	limitDefault   = 10
	offsetDefault  = 0
	timeoutDefault = 10
	baseUrlDefault = "https://api.telegram.org"
)

type TGClient interface {
	GetNextMessages() ([]Update, error)
	SendMessage(request SendMessageRequest) error
}

type ClientConfig struct {
	Limit   int
	Offset  int64
	Timeout int
	Token   string
	BaseUrl string
}

type MainTgClient struct {
	sync.Mutex
	urls   *ClientUrls
	client http.Client
}

func NewTGClient(config ClientConfig) (TGClient, error) {
	if config.Limit == 0 {
		config.Limit = limitDefault
	}
	if config.Offset == 0 {
		config.Offset = offsetDefault
	}
	if config.Timeout == 0 {
		config.Timeout = timeoutDefault
	}
	if config.BaseUrl == "" {
		config.BaseUrl = baseUrlDefault
	}
	if config.Token == "" {
		return nil, errors.New("no bot token value")
	}
	return &MainTgClient{
		Mutex:  sync.Mutex{},
		client: http.Client{},
		urls:   initClientUrls(config),
	}, nil
}

func (tc *MainTgClient) GetNextMessages() ([]Update, error) {
	getUpdatesUrl := tc.urls.formatGetUpdatesUrl()
	request, err := http.NewRequest(http.MethodGet, getUpdatesUrl, nil)
	if err != nil {
		return nil, err
	}

	response, err := tc.client.Do(request)
	if err != nil {
		return nil, err
	}

	byteResponse, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	result := UpdateResult{}
	err = json.Unmarshal(byteResponse, &result)
	if err != nil {
		return nil, err
	}
	if !result.Ok {
		return nil, errors.New("result not OK")
	}
	if len(result.Result) != 0 {
		tc.urls.Offset = result.Result[len(result.Result)-1].UpdateId + 1
	}
	return result.Result, nil
}

func (tc *MainTgClient) SendMessage(request SendMessageRequest) error {
	byteRequest, err := json.Marshal(request)
	if err != nil {
		return err
	}
	response, err := http.Post(tc.urls.sendMessageUrl, "application/json", bytes.NewBuffer(byteRequest))
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusOK {
		return errors.New("bad response: " + response.Status)
	}
	return nil
}
