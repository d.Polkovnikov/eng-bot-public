package configs

import (
	"gitlab.com/eng-bot/bot/common"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
)

func NewKeyboardConfig() models.KeyboardConfig {
	config := make(map[models.ApplicationState]interface{})
	config[common.BeginState] = telegramclient.ReplyKeyboardMarkup{
		Keyboard: [][]telegramclient.KeyboardButton{
			{telegramclient.KeyboardButton{Text: common.ADD_WORD_BUTTON}},
			{telegramclient.KeyboardButton{Text: common.SHOW_LIST_BUTTON}},
		},
		ResizeKeyboard: true,
	}
	return models.NewDefaultKeyboardConfig(config)
}
