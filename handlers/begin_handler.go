package handlers

import (
	"gitlab.com/eng-bot/bot/common"
	common2 "gitlab.com/eng-bot/bot/internal/botengine/models"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
)

type BeginHandler struct{}

func (h BeginHandler) Handle(update telegramclient.Update) (string, common2.ApplicationState, error) {
	switch update.Message.Text {
	case common.ADD_WORD_BUTTON:
		return "Введите слово, которое хотите добавить: ", common.AddWordState, nil
	default:
		return "Данный функционал еще не реализован\nВыберите желаемое действие", common.BeginState, nil
	}
}
