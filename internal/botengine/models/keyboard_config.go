package models

import "gitlab.com/eng-bot/bot/internal/telegramclient"

type KeyboardConfig interface {
	GetKeyboard(ApplicationState) interface{}
}

type DefaultKeyboardConfig struct {
	defaultKeyboard interface{}
	config          map[ApplicationState]interface{}
}

func NewDefaultKeyboardConfig(config map[ApplicationState]interface{}) KeyboardConfig {
	return &DefaultKeyboardConfig{
		defaultKeyboard: telegramclient.ReplyKeyboardHide{HideKeyboard: true},
		config:          config,
	}
}

func (kc *DefaultKeyboardConfig) GetKeyboard(state ApplicationState) interface{} {
	keyboard, ok := kc.config[state]
	if ok {
		return keyboard
	}
	return kc.defaultKeyboard
}
