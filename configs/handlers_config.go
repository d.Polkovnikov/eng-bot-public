package configs

import (
	"gitlab.com/eng-bot/bot/common"
	"gitlab.com/eng-bot/bot/handlers"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
)

func NewHandlerConfig() models.HandlerConfig {
	stateToHandler := make(map[models.ApplicationState]models.MessageHandler)
	stateToHandler[models.NewState] = handlers.NewUserHandler
	stateToHandler[common.AddWordState] = handlers.AddWordHandler
	stateToHandler[common.AddTranslationState] = handlers.AddTranslationUserHandler
	stateToHandler[common.BeginState] = handlers.BeginHandler{}
	return models.NewDefaultHandlerConfig(stateToHandler).(models.HandlerConfig)
}
