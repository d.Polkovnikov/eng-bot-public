package models

import "errors"

type HandlerConfig interface {
	GetHandler(ApplicationState) (MessageHandler, error)
}

type DefaultHandlerConfig struct {
	stateToHandlers map[ApplicationState]MessageHandler
}

func (d *DefaultHandlerConfig) GetHandler(state ApplicationState) (MessageHandler, error) {
	handler, ok := d.stateToHandlers[state]
	if ok {
		return handler, nil
	}
	return nil, errors.New("Unhandled Application state: " + string(state))
}

func NewDefaultHandlerConfig(stateToHandlers map[ApplicationState]MessageHandler) HandlerConfig {
	return &DefaultHandlerConfig{stateToHandlers: stateToHandlers}
}
