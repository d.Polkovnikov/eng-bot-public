package common

import "gitlab.com/eng-bot/bot/internal/botengine/models"

const (
	ADD_WORD_BUTTON  = "Добавить слово"
	SHOW_LIST_BUTTON = "Показать все слова"
)

const (
	BeginState          models.ApplicationState = "BEGIN"
	AddWordState        models.ApplicationState = "ADD_WORD"
	AddTranslationState models.ApplicationState = "ADD_TRANSLATION"
)
