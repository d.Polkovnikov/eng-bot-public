package models

import "gitlab.com/eng-bot/bot/internal/telegramclient"

type MainConfig struct {
	KeyboardConfig KeyboardConfig
	HandlerConfig  HandlerConfig
	TelegramConfig telegramclient.ClientConfig
}
