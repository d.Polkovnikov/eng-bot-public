package core

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
	"gitlab.com/eng-bot/bot/internal/telegramclient/mocks"
	"reflect"
	"testing"
	"time"
)

const defaultMsg = "MSG"
const defaultToken = "TOKEN"

var (
	testKeyboardConfig = models.NewDefaultKeyboardConfig(
		map[models.ApplicationState]interface{}{
			models.NewState: telegramclient.ReplyKeyboardMarkup{},
		},
	)

	testHandlerConfig = models.NewDefaultHandlerConfig(
		map[models.ApplicationState]models.MessageHandler{
			models.NewState: models.NewDefaultMessageHandler(defaultMsg, models.NewState),
		},
	)

	defaultConfig = models.MainConfig{
		KeyboardConfig: testKeyboardConfig,
		HandlerConfig:  testHandlerConfig,
		TelegramConfig: telegramclient.ClientConfig{Token: defaultToken},
	}
)

func TestCreateServer(t *testing.T) {
	tests := []struct {
		name     string
		config   models.MainConfig
		wantErr  bool
		doBefore func()
		doAfter  func()
	}{
		{
			name:     "CreateServer should return server without err",
			config:   defaultConfig,
			wantErr:  false,
			doBefore: func() {},
			doAfter:  func() {},
		},
		{
			name: "CreateServer should return err when no NewState handler",
			config: models.MainConfig{
				KeyboardConfig: models.NewDefaultKeyboardConfig(map[models.ApplicationState]interface{}{}),
				HandlerConfig:  models.NewDefaultHandlerConfig(map[models.ApplicationState]models.MessageHandler{}),
				TelegramConfig: telegramclient.ClientConfig{},
			},
			wantErr: true,
		},
		{
			name: "CreateServer should return err when no TG token",
			config: models.MainConfig{
				KeyboardConfig: testKeyboardConfig,
				HandlerConfig:  testHandlerConfig,
				TelegramConfig: telegramclient.ClientConfig{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := CreateServer(tt.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateServer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestServer_MessageHandler_working(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockedClient := mocks.NewMockTGClient(ctrl)
	server := createTestServer(mockedClient, defaultConfig)
	request := createTestRequest("TEXT", "10")

	t.Run("should handle message from channel", func(t *testing.T) {
		testContext, closeContext := context.WithCancel(context.Background())
		defer closeContext()
		mockedClient.EXPECT().
			SendMessage(gomock.Eq(request)).
			Times(1)

		go server.startHandlingMessages(testContext)
		server.messages <- createTestRequest("TEXT", "10")
	})

	t.Run("should add error to errors chan", func(t *testing.T) {
		duration, _ := time.ParseDuration("5s")
		testContext, closeContext := context.WithTimeout(context.Background(), duration)
		defer closeContext()
		mockedClient.EXPECT().
			SendMessage(gomock.Eq(request)).
			Return(errors.New("TEST_ERROR"))

		go server.startHandlingMessages(testContext)
		server.messages <- createTestRequest("TEXT", "10")

		select {
		case testError := <-server.errors:
			if testError.Error() != "TEST_ERROR" {
				t.Errorf("SendMessage() not handle error")
			}
		case <-testContext.Done():
			t.Errorf("timout error")
		}
	})
}

func TestServer_UpdatesHandler_working(t *testing.T) {

	t.Run("should add update to updates chan", func(t *testing.T) {

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockedClient := mocks.NewMockTGClient(ctrl)
		server := createTestServer(mockedClient, defaultConfig)

		testContext, closeContext := context.WithCancel(context.Background())
		testUpdates := createTestUpdate("test")
		defer closeContext()

		first := mockedClient.
			EXPECT().
			GetNextMessages().
			Return(testUpdates, nil).
			Times(1)
		mockedClient.EXPECT().
			GetNextMessages().
			Return([]telegramclient.Update{}, nil).
			AnyTimes().
			After(first)

		go server.startReadUpdates(testContext)

		testUpdate := <-server.updates
		if !reflect.DeepEqual(testUpdate, testUpdates[0]) {
			t.Errorf("Expected update not equlas real. Real: %v, expected: %v", testUpdate, testUpdates[0])
		}
	})

	t.Run("should add error to err chan", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockedClient := mocks.NewMockTGClient(ctrl)
		server := createTestServer(mockedClient, defaultConfig)
		duration, _ := time.ParseDuration("5s")
		testContext, closeContext := context.WithTimeout(context.Background(), duration)
		defer closeContext()
		first := mockedClient.
			EXPECT().
			GetNextMessages().
			Return([]telegramclient.Update{}, errors.New("TEST_ERROR")).
			Times(1)
		mockedClient.EXPECT().
			GetNextMessages().
			Return([]telegramclient.Update{}, nil).
			AnyTimes().
			After(first)

		go server.startReadUpdates(testContext)

		select {
		case testError := <-server.errors:
			if testError.Error() != "TEST_ERROR" {
				t.Errorf("SendMessage() not handle error")
			}
		case <-testContext.Done():
			t.Errorf("timout error")
		}
	})
}

func TestServer_HandlingUpdates_working(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockedClient := mocks.NewMockTGClient(ctrl)
	server := createTestServer(mockedClient, defaultConfig)

	t.Run("should handle updates from channel", func(t *testing.T) {
		duration, _ := time.ParseDuration("5s")
		testContext, closeContext := context.WithTimeout(context.Background(), duration)
		defer closeContext()

		go server.startHandlingUpdates(testContext)
		server.updates <- telegramclient.Update{
			UpdateId: 1,
			Message: telegramclient.Message{
				Id:   10,
				From: telegramclient.User{Id: 100},
			},
		}

		select {
		case <-testContext.Done():
			t.Errorf("timout error")
		case msg := <-server.messages:
			if msg.Text != defaultMsg {
				t.Errorf("Expected message doesn't match actual message. Expexted: %s. Actual: %s", defaultMsg, msg.Text)
			}
			if msg.ChatId != "100" {
				t.Errorf("Expected chatId doesn't match actual chatId. Expexted: %s. Actual: %s", "100", msg.ChatId)
			}
			if !reflect.DeepEqual(msg.ReplyMarkup, telegramclient.ReplyKeyboardMarkup{}) {
				t.Errorf("Expected ReplyMarkupId doesn't match actual ReplyMarkupId. Actual: %s", msg.ReplyMarkup)
			}
		}
	})
}

func createTestServer(client telegramclient.TGClient, config models.MainConfig) Server {
	mainHandler, err := NewMainHandler(
		config.KeyboardConfig,
		config.HandlerConfig,
	)
	if err != nil {
		panic(err.Error())
	}
	return Server{
		requestHandlers: mainHandler,
		tgClient:        client,
		updates:         make(chan telegramclient.Update),
		errors:          make(chan error),
		messages:        make(chan telegramclient.SendMessageRequest),
	}
}

func createTestRequest(msg string, chatId string) telegramclient.SendMessageRequest {
	return telegramclient.SendMessageRequest{
		ChatId:         chatId,
		Text:           msg,
		MessageToReply: 0,
		ReplyMarkup:    telegramclient.ReplyKeyboardMarkup{},
	}
}

func createTestUpdate(msg string) []telegramclient.Update {
	return []telegramclient.Update{
		{
			UpdateId: 1,
			Message: telegramclient.Message{
				Id:   1,
				From: telegramclient.User{},
				Date: 1,
				Text: msg,
			},
		},
	}
}
