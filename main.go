package main

import (
	"context"
	"fmt"
	"gitlab.com/eng-bot/bot/configs"
	"gitlab.com/eng-bot/bot/internal/botengine/core"
	"gitlab.com/eng-bot/bot/internal/botengine/models"
	"gitlab.com/eng-bot/bot/internal/telegramclient"
	"os"
	"time"
)

const (
	TokenEnvVarName = "TG_BOT_TOKEN"
)

func main() {
	duration, err := time.ParseDuration("10m")
	if err != nil {
		panic(err)
	}
	ctx, closeContext := context.WithTimeout(context.Background(), duration)
	defer closeContext()

	server, err := core.CreateServer(models.MainConfig{
		KeyboardConfig: configs.NewKeyboardConfig(),
		HandlerConfig:  configs.NewHandlerConfig(),
		TelegramConfig: telegramclient.ClientConfig{Token: os.Getenv(TokenEnvVarName)},
	})
	if err != nil {
		panic(err.Error())
	}
	server.Start(ctx)

	select {
	case <-ctx.Done():
		fmt.Print("CLOSE")
	}
}
